<?php
    include_once("conexion.php");

    pg_send_query($conn, "SELECT id_topic AS ID, name_topic AS title
                            FROM topic");

    $result = pg_get_result($conn);
    if($result){

        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
        if ($state == 0){

            $topics = pg_fetch_all($result);
            $res = array("success"=>true,
                            "topics"=>$topics);

        }else{
            $res = array("success"=>false,
                            "msg"=>"An error ocurred while showing the topic list. Try again.");
        }

    }

    echo json_encode($res);


?>