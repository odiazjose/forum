<?php
    include_once("conexion.php");

    $id_thread = isset($_POST['thread']) ? $_POST['thread'] : '';

    $ready = true;

    //Fetch thread information
    pg_send_query($conn, "SELECT id_thread AS threadID, date_thread AS threadDate, title_thread AS threadTitle, thread.text_thread AS message, thread.id_user AS authorID, username_user AS author 
                            FROM thread RIGHT JOIN member ON thread.id_user = CAST(member.id_user AS TEXT)
                            WHERE id_thread = '$id_thread'");

    $result = pg_get_result($conn);

    if($result){
        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);

        if($state==0){

            $thread = pg_fetch_all($result)[0];

        }else{
            $ready = false;
        }
    }

    //Fetch comments related to thread
    pg_send_query($conn, "SELECT id_com AS commentID, id_thread AS threadID, comment.id_user AS userID, username_user AS username, date_com AS date, text_com AS message, coalesce(array_length(likes_com, 1), 0) as Likes
                                FROM comment RIGHT JOIN member ON comment.id_user = member.id_user
                                WHERE id_thread = '$id_thread' ORDER BY id_com");

    $result = pg_get_result($conn);

    if($result){
        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);

        if($state==0){

            $comments = pg_fetch_all($result);

        }else{
            $ready = false;
        }
    }

    //Build response and send
    if($ready){
        $res = array("success"=>true,
                        "original"=>$thread,
                        "comments"=>$comments);
    }else{
        $res = array("success"=>false,
                        "msg"=>"An error has ocurred while showing the thread. Please try again.");
    }

    echo json_encode($res);

?>