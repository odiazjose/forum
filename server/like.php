<?php
    include_once("conexion.php");

    session_start();

    $id_user = $_SESSION['id'];
    $id_com = isset($_POST['commentid']) ? $_POST['commentid'] : '';



    //Check for a like in this comment, by this user.
    /*
     * To reduce latency, I recommend you try to avoid this at all in the client
     * so this whole module won't have to be excecuted
     */

    pg_send_query($conn, "SELECT array_to_string(likes_com, ',') FROM comment
                            WHERE id_com = '$id_com'");

    $result = pg_get_result($conn);

    if($result){
        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);

        if($state == 0){
            $rows = pg_fetch_all($result);
            $string = $rows['0'];

            $likes = explode(',', $string['array_to_string']);

            $liked = in_array($id_user, $likes) ? true : false;

        }else{
            $res = array("liked"=>false,
                            "msg"=>"An error has ocurred while liking this comment.");
        }
    }

    if($liked){
        $res = array("liked"=>false,
                        "msg"=>"You already like this comment!");
    }else if(!$liked){
        pg_send_query($conn, "UPDATE comment
                                SET likes_com = likes_com || ARRAY[$id_user]
                                WHERE id_com = '$id_com'");

        $result = pg_get_result($conn);
        if($result){
            $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
            if($state == 0)
                $res = array("liked"=>true);
            else
                $res = array("liked"=>false,
                                "msg"=>"An error has ocurred while liking this comment.");
        }
    }

    echo json_encode($res);

?>