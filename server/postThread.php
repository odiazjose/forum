<?php
    include_once("conexion.php");

    session_start();

    $id_user = $_SESSION['id'];
    $date = date("Y-m-d");
    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $text = isset($_POST["text"]) ? $_POST["text"] : "";
    $topics_post = isset($_POST["topics"]) ? $_POST["topics"] : "";

    $ready = true;
    $thread_id = 0;

    //Make sure topics is an array
    $topics = array_map('intval', explode(',', str_replace("[", "", $topics_post)));

    //Insert thread in table thread
    $query = pg_send_query($conn, "INSERT INTO
                                    thread(id_user, title_thread, date_thread, text_thread)
                                    VALUES('$id_user', '$title', '$date', '$text')
                                    RETURNING id_thread");

    //Check the result of the thread query
    $result = pg_get_result($conn);
    if($result) {
        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
        if($state == 0){
            $row = pg_fetch_row($result);
            $thread_id = $row['0'];
        }else{
            $res = array("posted"=>false,
                         "msg"=>"An error has ocurred while posting the thread. Try again.");

            $thread_id = -1;
            $ready = false;
        }
    }

    //Iterate over topics to insert the relationship in the topic_thread table
    pg_prepare($conn, "topic_query", "INSERT INTO
                                        topic_thread(id_thread, id_topic)
                                        VALUES($1, $2)");

    if($thread_id != -1){
        foreach($topics as $topic){
            pg_send_execute($conn, "topic_query", array($thread_id, $topic));

            $result = pg_get_result($conn);

            if($result) {

                $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
                if($state != 0){
                    $ready = false;
                    if($state==23503){
                        $res = array("posted"=>false,
                                        "msg"=>"No existe un tema que concuerde con el ingresado.");
                    }else{
                        $res = array("posted"=>false,
                                        "msg"=>"An error has ocurred while posting the thread. Try again.");
                    }

                    //If there was an error, delete thread (it will cascade to topic_thread)
                    pg_query($conn, "DELETE FROM thread
                                                WHERE id_thread = '$thread_id'");

                    break;
                }
            }
        }
    }

    if ($ready){
        $res = array("posted"=>true, "thread"=>$thread_id);
    }

    echo json_encode($res);

?>