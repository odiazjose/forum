<?php
    include_once("conexion.php");

    $email_user = isset($_POST["email"]) ? $_POST["email"] : '';
    $pass_user = isset($_POST["pass"]) ? $_POST["pass"] : '';
    $name_user = isset($_POST["name"]) ? $_POST["name"] : '';
    $lastName_user = isset($_POST["lastName"]) ? $_POST["lastName"] : '';
    $userName_user = isset($_POST["username"]) ? $_POST["username"] : '';

    pg_send_query($conn, "INSERT INTO
                              member(email_user, pass_user, name_user, lastname_user, username_user)
                              VALUES('$email_user', '$pass_user', '$name_user', '$lastName_user', '$userName_user')");

    $result = pg_get_result($conn);
    if($result) {
        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
        if($state == 0){
            $res = array("registered"=>true);
        }else{
            if ($state == "23505"){
                $res = array("registered"=>false,
                             "msg"=>"There's already another account registered to that email and/or username.");
            }else{
                $res = array("registered"=>false,
                             "msg"=>"An error has ocurred during the register process. Please try again.");
            }
        }
    }

    echo json_encode($res);
?>
