<?php
    include_once("conexion.php");

    $topic = isset($_POST['category']) ? $_POST['category'] : '';

    pg_send_query($conn, "SELECT thread.id_thread AS threadID, date_thread AS threadDate, title_thread AS title, username_user AS author
                            FROM thread RIGHT JOIN member ON thread.id_user = CAST(member.id_user AS TEXT), topic_thread
                            WHERE topic_thread.id_topic = '$topic'
                            AND topic_thread.id_thread = thread.id_thread");

    $result = pg_get_result($conn);
    if($result){

        $state = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
        if ($state == 0){

            $threads = pg_fetch_all($result);
            $res = array("success"=>true,
                            "threads"=>$threads);

        }else{
            $res = array("success"=>false,
                            "msg"=>"Error mostrando lista de hilos. Intente de nuevo");
        }

    }

    echo json_encode($res);


?>