var Thread = function(originalPost, comments, commentCallback, likeCallback){
	var thread = document.createElement('div');
	var body = document.createElement('div');

	var opSection = document.createElement('div');
	var responseSection = document.createElement('div');
	var postSection = document.createElement('div');

	opSection.innerHTML = '<h3>' + originalPost.threadtitle + '</h3> by ' + originalPost.author + ' on ' + originalPost.threaddate + '.';
	opSection.innerHTML += '<br><p>' + originalPost.message + '</p>';

	for(var i = 0; i < comments.length; i++){
		var comment = document.createElement('div');

		var profile = document.createElement('p');
		var content = document.createElement('p');

		profile.innerHTML = '<h3>' + comments[i].username + '</h3> said on ' + comments[i].date +'...';
		content.innerHTML = comments[i].message;

		comment.className = 'thread-comment';

		comment.appendChild(profile);
		comment.appendChild(content);

		var commentOption = document.createElement('div');
		var option = document.createElement('span');

		option.innerHTML = 'Like! : ' + comments[i].likes;
		option.authorID = comments[i].commentid;

		option.onclick = function(){
			var data = {commentid: this.authorID, thread: originalPost.threadid};

			likeCallback(data);
		};

		commentOption.className = 'thread-comment-options';

		commentOption.appendChild(option);

		comment.appendChild(commentOption);

		responseSection.appendChild(comment);
	}

	var commentArea = document.createElement('textarea');

	var commentButton = document.createElement('button');
	commentButton.type = 'button';
	commentButton.innerHTML = 'Send comment';

	commentButton.onclick = function(){
		var textArea = document.getElementsByTagName ("textarea");
		var commentData = {thread: originalPost.threadid, message: textArea[0].value};

		 commentCallback(commentData);
	};

	postSection.innerHTML = '<h3>Leave a comment below</h3>';
	postSection.appendChild(commentArea);
	postSection.innerHTML += '<br><br>';
	postSection.appendChild(commentButton);

	thread.className = 'thread';
	body.className = 'thread-body';
	opSection.className = 'thread-op';
	responseSection.className = 'thread-response-section';
	postSection.className = 'thread-post-section';

	body.appendChild(opSection);
	body.appendChild(responseSection);
	body.appendChild(postSection);

	thread.appendChild(body);

	return thread;
};