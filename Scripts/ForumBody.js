var ForumBody = function(){
	var forumBody = document.createElement('div');
	var head = document.createElement('div');
	var body = document.createElement('div');

	var quickNavigation = document.createElement('div');
	var quickNavVisible = true;

	forumBody.className = 'forum';
	head.className = 'forum-body-head';
	body.className = 'forum-body';
	quickNavigation.className = 'forum-nav';

	head.appendChild(quickNavigation);

	forumBody.appendChild(head);
	forumBody.appendChild(body);

	var navLevels = [];

	forumBody.addQuickNavigation = function(directory, callback, callbackData){
		var level = document.createElement('div');

		navLevels.push(level);

		level.innerHTML = directory + ' > ';

		level.onclick = function(){
			var newLevels = [];
			var counter = 0;

			for(var i = 0; i < navLevels.length; i++){
				newLevels[counter++] = navLevels[i];
				if(navLevels[i] === this) break;
			}

			navLevels = newLevels;

			quickNavigation.innerHTML = '';
			for(var i = 0; i < navLevels.length; i++){
				quickNavigation.appendChild(navLevels[i]);
			}

			callback(callbackData);
		};

		quickNavigation.appendChild(level);
	};

	forumBody.setQuickNavigation = function(directories, callback){
		navLevels = [];
		if(!quickNavVisible) head.appendChild(quickNavigation);

		quickNavigation.innerHTML = '';

		for(var i = 0; i < directories.length; i++){
			this.addQuickNavigation(directories[i], callback[i], 0);
		}
	};
	
	forumBody.addSection = function(sectionData, callback){
		var callbackData = {category: sectionData.id};

		var section = document.createElement('div');
		var image = new Image();
		var titleDiv = document.createElement('div');
		var title = document.createElement('h3');

		image.src = 'Images/H.png';
		title.innerHTML = sectionData.title;

		section.onclick = function(){
			body.innerHTML = '';

			forumBody.addQuickNavigation(sectionData.title, callback, callbackData);
			callback(callbackData);
		};

		section.className = 'forum-section';

		titleDiv.appendChild(title);
		section.appendChild(image);
		section.appendChild(titleDiv);

		body.appendChild(section);
	};

	forumBody.addThread = function(threadData, callback){
		var callbackData = {thread: threadData.threadid};

		var thread = document.createElement('div');
		var title = document.createElement('h3');
		var OP = document.createElement('h5');
		
		title.innerHTML = threadData.title;
		OP.innerHTML = threadData.author;

		thread.onclick = function(){
			callback(callbackData);
		}

		thread.className = 'forum-thread';

		thread.appendChild(title);
		thread.appendChild(OP);

		body.appendChild(thread);
	};

	forumBody.clear = function(){
		body.innerHTML = '';
	};

	forumBody.allClear = function(){
		quickNavVisible = false;

		head.innerHTML = '';
		body.innerHTML = '';
	};

	forumBody.append = function(object){
		body.appendChild(object);
	};

	return forumBody;
};