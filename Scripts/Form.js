var Form = function(title, inputs, outputNames, callback){
	var form = document.createElement('div');
	var head = document.createElement('div');
	var body = document.createElement('div');

	head.innerHTML = '<h3>'+ title + '</h3>';

	var inputSection = document.createElement('div');
	var buttonSection = document.createElement('div');

	for(var i = 0; i < inputs.length; i++){
		var input = document.createElement('input');

		if(Array.isArray(inputs[i])){
			input.type = inputs[i][1];

			inputSection.innerHTML += inputs[i][0] + ': <br>';
		}else{
			input.type = 'text';

			inputSection.innerHTML += inputs[i] + ': <br>';
		}

		inputSection.appendChild(input);

		inputSection.innerHTML += '<br>';
	}

	var sendButton = document.createElement('button');
	sendButton.type = 'button';

	sendButton.innerHTML = 'Send';

	sendButton.onclick = function(){
		var input = document.getElementsByTagName ("input");

		var data = '{';

		for(var i = 0; i < input.length; i++){
			data += '"' + outputNames[i] + '"' + ': "' + input[i].value + '"';

			if(i + 1 < input.length) data += ', ';
		}

		data += '}';

		callback(JSON.parse(data));
	};

	buttonSection.appendChild(sendButton);

	body.appendChild(inputSection);
	body.appendChild(buttonSection);

	head.className = 'form-head';
	body.className = 'form-body';
	form.className = 'form';

	form.appendChild(head);
	form.appendChild(body);

	return form;
};