var ThreadForm = function(title, inputs, inputType, outputNames, categories, callback){
	var form = document.createElement('div');
	var head = document.createElement('div');
	var body = document.createElement('div');

	head.innerHTML = '<h3>'+ title + '</h3>';

	var inputSection = document.createElement('div');
	var buttonSection = document.createElement('div');

	for(var i = 0; i < inputs.length; i++){
		var input = document.createElement(inputType[i]);
		input.className = 'form-input';

		if(Array.isArray(inputs[i])){
			input.type = inputs[i][1];

			inputSection.innerHTML += inputs[i][0] + ': <br>';
		}else{
			input.type = 'text';

			inputSection.innerHTML += inputs[i] + ': <br>';
		}

		inputSection.appendChild(input);

		inputSection.innerHTML += '<br>';
	}

	inputSection.innerHTML += '<h3>Categories</h3>';

	for(var i = 0; i < categories.length; i++){
		var checkBox = document.createElement('input');
		checkBox.type = 'checkbox';
		checkBox.className = 'form-category-checkbox';

		checkBox.value = categories[i].id;

		inputSection.appendChild(checkBox);
		inputSection.innerHTML += categories[i].title + '<br>';
	}

	var sendButton = document.createElement('button');
	sendButton.type = 'button';

	sendButton.innerHTML = 'Send';

	sendButton.onclick = function(){
		var input = document.getElementsByClassName('form-input');

		var data = '{';

		for(var i = 0; i < input.length; i++){
			data += '"' + outputNames[i] + '"' + ': "' + input[i].value + '"';

			data += ', ';
		}

		data += '"topics": [';

		var cat = document.getElementsByClassName('form-category-checkbox');
		var checked = [];
		var counter = 0;
		for(var i = 0; i < cat.length; i++){
			if(cat[i].checked){
				checked[counter++] = cat[i].value;
			}
		}

		for(var i = 0; i < checked.length; i++){
			data += checked[i];
			if(i + 1 < checked.length) data += ', ';
			else data += ']';
		}

		data += '}';

		console.log(data);
		
		callback(JSON.parse(data));
	};

	buttonSection.appendChild(sendButton);

	body.appendChild(inputSection);
	body.appendChild(buttonSection);

	head.className = 'thread-form-head';
	body.className = 'thread-form-body';
	form.className = 'thread-form';

	form.appendChild(head);
	form.appendChild(body);

	return form;
};