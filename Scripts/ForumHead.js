var ForumHead = function(logoURI, buttons, callbacks){
	var forum_Head = document.createElement('div');
	var body = document.createElement('div');

	var logoSection = document.createElement('div');
	var buttonSection = document.createElement('div');

	var logo = new Image();
	logo.src = logoURI;

	logoSection.appendChild(logo);

	for(var i = 0; i < buttons.length; i++){
		var button = document.createElement('div');
		button.className = 'forum-head-button';

		button.innerHTML = buttons[i];

		button.onclick = callbacks[i];

		buttonSection.appendChild(button);
	}

	body.appendChild(logoSection);
	body.appendChild(buttonSection);

	forum_Head.appendChild(body);

	logoSection.className = 'forum-logo';
	buttonSection.className = 'forum-head-button-section';
	body.className = 'forum-head-body';
	forum_Head.className = 'forum-head';

	forum_Head.setLogoLink = function(callback){
		logo.onclick = callback;
	};

	return forum_Head;
};