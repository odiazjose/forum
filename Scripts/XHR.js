XHR = function(){
    var xhr = new XMLHttpRequest();

    this.get = function(url, asynchronousMode, callback){
        xhr.open("GET", url, asynchronousMode);

        xhr.onreadystatechange = function(){
                if(xhr.status === 200 && xhr.readyState === 4){
                    var json = JSON.parse(xhr.responseText);
                    
                    callback(json);
                }
        };

        xhr.send();
    };

    this.post = function(url, asynchronousMode, callback, json){
        var parameters = "";
        var firstParameter = true;

        for(var property in json){
            if(firstParameter === false) parameters += "&";
            else firstParameter = false;

            parameters += property + "=" + this.getPropertyValue(json, property);
        }
        
        xhr.open("POST", url, asynchronousMode);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function(){
            if(xhr.status === 200 && xhr.readyState === 4){
                var jsonResponse = JSON.parse(xhr.responseText);
                
                callback(jsonResponse);
            }
        };
        
        xhr.send(parameters);
    };

    this.getPropertyValue = function(object, property){
        var propertyType = typeof(object[property]);

        if(propertyType === "string" || propertyType === "number" || propertyType === "boolean"){
            return object[property];
        }else if(Array.isArray(object[property])){
            return JSON.stringify(object[property]);
        }else if(propertyType === "object"){
            return JSON.stringify(object[property]);
        }else{
            return null;
        }
    };
};